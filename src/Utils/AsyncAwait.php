<?php declare(strict_types = 1);

namespace Selex\Utils;

use Rx\Observable;
use React\EventLoop\LoopInterface;
use React\Promise\PromiseInterface;

class AsyncAwait {

    protected $exec;
    protected static $loop;

    public function __construct(Callable $generator, $return = 'async') {
        $creator = static::class . '::' . $return;
        $this->exec = $creator($generator);
    }

    public function __invoke(...$args) {
        return call_user_func_array($this->exec, $args);
    }

    public static function setEventLoop(LoopInterface $loop = null) {
        static::$loop = RxFactoryManager::setAsyncLoop($loop);
    }

    public static function async(Callable $generator) {
        return function (...$args) use ($generator) {
            $iterator = call_user_func_array($generator, $args);
            $next = function ($data = null) use ($iterator, &$next) {
                $value = $iterator->current();
                if ($iterator->valid() || ($value instanceof PromiseInterface)) {
                    return $value->then(function ($value) use ($iterator, &$next) {
                        $iterator->send($value);
                        return call_user_func($next, $value);
                    });
                }
                return \React\Promise\resolve($iterator->getReturn());
            };
            return \React\Promise\resolve(call_user_func($next));
        };
    }

    public static function observe(Callable $generator, LoopInterface $loop = null) {
        if (!isset(static::$loop) || isset($loop)) {
            static::setEventLoop($loop);
        }

        $func = static::async($generator);
        return function (...$args) use ($func) {
            return Observable::fromPromise(call_user_func_array($func, $args));
        };
    }

}