<?php declare(strict_types = 1);

namespace Selex\Utils;

if (!function_exists('Selex\\Utils\\isAssociative')) {
    function isAssociative(array $arr): bool {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}
