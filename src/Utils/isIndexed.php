<?php declare(strict_types = 1);

namespace Selex\Utils;

if (!function_exists("Selex\\Util\\isIndexed")) {
    require_once __DIR__ . '/isAssociative.php';
    function isIndexed(array $arr): bool {
        return !isAssociative($arr);
    }
}
