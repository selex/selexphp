<?php declare(strict_types = 1);

namespace Selex\Utils;

use React\EventLoop\LoopInterface;
use React\EventLoop\Factory;
use Rx\Scheduler;
use Rx\Scheduler\EventLoopScheduler;

class RxFactoryManager {

    protected static $isset = false;
    protected static $loop;

    function setAsyncLoop(LoopInterface $loop = null) {
        if (static::$isset && !isset($loop)) {
            return static::$loop;
        }

        $loop = $loop ?? (static::$loop ?? Factory::create());
        static::$loop = $loop;

        Scheduler::setDefaultFactory(function() {
            return Scheduler::getImmediate();
        });

        Scheduler::setAsyncFactory(function() use ($loop) {
            return new EventLoopScheduler($loop);
        });

        static::$isset = true;
        return $loop;
    }

}
