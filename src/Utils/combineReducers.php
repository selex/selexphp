<?php declare(strict_types = 1);

namespace Selex\Utils;

require_once __DIR__ . '/isIndexed.php';

use Closure;
use Qaribou\Collection\ImmArray;

function combineReducers($reducers): Closure {
    if (is_callable($reducers)) return $reducers;

    if (is_array($reducers) && isIndexed($reducers)) {
        return ImmArray::fromArray($reducers)
            ->map('Selex\\Utils\\combineReducers')
            ->reduce(function (Callable $reduce, Callable $next) {
                return function ($state, $action) use ($reduce, $next) {
                    return call_user_func(
                        $reduce,
                        call_user_func($next, $state, $action),
                        $action
                    );
                };
            }, function ($state) { return $state; });
    }

    if (is_scalar($reducers)) {
        return function () use ($reducers) {
            return $reducers;
        };
    }

    return function ($state = [], $action) use ($reducers) {
        $next = [];
        foreach ($reducers as $key => $value) {
            $keyState = is_array($state) ? ($state[$key] ?? null) : ($state->$key ?? null);
            $next[$key] = call_user_func(combineReducers($value), $keyState, $action);
        }
        return is_array($reducers) ? $next : (object) $next;
    };
}
