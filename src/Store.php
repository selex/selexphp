<?php declare(strict_types = 1);
namespace Selex;

require_once __DIR__ . '/Utils/combineReducers.php';

use Rx\Subject\Subject;
use React\EventLoop\LoopInterface;
use Selex\Utils;

class Store {

    protected $actionSubject;
    // protected $reducerSubject;
    protected $observable;
    protected $currentState;
    protected $dispatcher;
    protected $actions = [];
    protected static $loop;

    protected function __construct($rootReducer, $initialState) {
        $this->actionSubject = new Subject();
        // $this->reducerSubject = new Subject();
        
        $this->observable = $this->actionSubject->asObservable()
            ->map(function ($action) use ($rootReducer) {
                return function ($state) use ($action, $rootReducer) {
                    return call_user_func(Utils\combineReducers($rootReducer), $state, $action);
                };
            })
            // ->withLatestFrom(
            //     [$this->reducerSubject->asObservable()->startWith($rootReducer)],
            //     function ($action, $reducer) {
            //         return function ($state) use ($action, $reducer) {
            //             return call_user_func(combineReducers($reducer), $state, $action);
            //         };
            //     }
            // )
            ->scan(function ($state, $apply) {
                return call_user_func($apply, $state);
            }, $initialState);

        $this->observable->subscribe(function ($nextState) {
            $this->currentState = $nextState;
        });

        $this->dispatcher = function (\stdClass $action) {
            $this->actionSubject->onNext($action);
            return $action;
        };
    }

    public function createAction(string $type, Callable $payload = null, Callable $meta = null) {
        $reducer = $payload ?: function ($value = null) {
            return $value;
        };

        $dispatcher = $this->dispatcher;

        $reduceAction = function (...$args) use ($dispatcher, $type, $reducer, $meta) {
            $addMeta = ($meta ? [ 'meta' => call_user_func_array($meta, $args) ] : []);
            $action = (object) array_merge([
                'type' => $type,
                'payload' => call_user_func_array($reducer, $args)
            ], $addMeta);
            call_user_func($dispatcher, $action);
        };

        $this->actions = array_merge($this->actions, [
            $type => $reduceAction
        ]);

        return $this->actions[$type];
    }

    public function select(Callable $selector, Callable $keySelect = null, Callable $comparator = null) {
        return $this->observable->map($selector)
            ->distinctUntilChanged($keySelect, $comparator)
            ->asObservable();
    }

    public function subscribe(Callable $callback) {
        return $this->observable->subscribe($callback);
    }

    protected function createActionDispatcher(Callable $reducer) {
        $dispatcher =& $this->dispatcher;
        return function (...$args) use ($reducer, $dispatcher) {
            return call_user_func(
                $dispatcher,
                call_user_func_array($reducer, $args)
            );
        };
    }

    public function currentState() {
        return $this->currentState;
    }

    public function getState() {
        return $this->currentState;
    }

    public function observe() {
        return $this->observable;
    }

    public static function create($rootReducer = null, $initialState = null, LoopInterface $loop = null) {
        $class = static::class;

        // if (is_callable($middleware)) {
        //     $createStore = "{$class}::create";
        //     $createStore = call_user_func($middleware, $createStore);
        //     return call_user_func($createStore, $rootReducer, $initialState);
        // }

        if (!isset(static::$loop) || isset($loop)) {
            static::$loop = Utils\RxFactoryManager::setAsyncLoop($loop);
        }

        $defaultReducer = function($state) {
            return $state;
        };
        $defaultState = (object) [];

        return new $class($rootReducer ?: $defaultReducer, $initialState ?: $defaultState);
    }

}
